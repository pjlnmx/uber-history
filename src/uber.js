var Uber = require('node-uber'),
	uuid = require('uuid/v4'),
	redis = require('redis'),
	bluebird = require('bluebird');

bluebird.promisifyAll(redis);

const CLIENT_ID = process.env.CLIENT_ID;
const SERVER_TOKEN = process.env.SERVER_TOKEN;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URI = process.env.REDIRECT_URI;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient( {
	host: REDIS_HOST
});
const uber = new Uber({
	client_id: CLIENT_ID,
	client_secret: CLIENT_SECRET,
	server_token: SERVER_TOKEN,
	redirect_uri: REDIRECT_URI,
	name: 'History Project',
	sandbox: true,
	language: 'en_US'
});



var view = async function(req, res, next) {
	var template, title;
	var guid = req.cookies.user || uuid();
	var userData = await client.hgetallAsync(guid);
	var data = {
		user: guid
	};

	if (!userData) {
		data.loginUrl = uber.getAuthorizeUrl(['history','profile']);
		title = 'Log In - Uber History';
		template = 'login.vue'
	} else {
		title = 'Uber History';
		template = 'main.vue'
		data.userData = {}
		Object.keys(userData).map(function(key) {
			try {
				data.userData[key] = JSON.parse(userData[key]);
			} catch (e) {
				data.userData[key] = userData[key];
			}
		});
		data.drawer = false;
		data.map = null;
		data.tileLayer = null;
		data.layers = [];
		data.headers = [];
		data.trips = [];
		data.mobile = false;
		data.dialog = false;
	}

	
	req.vueOptions = {
        head: {
            title: title,
            metas: [
                { property:'og:title', content: title},
				{ name: 'twitter:title', content: title},
				{ name: 'viewport', content: 'width=device-width,initial-scale=1' }
			],
			scripts: [
				{ src: 'https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js' },
				{ src: 'https://unpkg.com/vue-cookies@1.5.12/vue-cookies.js'},
				{ src: 'https://unpkg.com/leaflet@1.2.0/dist/leaflet.js' },
				{ src: 'https://unpkg.com/moment@2.24.0/moment.js' },
				{ src: 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js' }
			],
			styles: [
				{ style: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' },
				{ style: 'https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css' },
				{ style: 'https://unpkg.com/leaflet@1.2.0/dist/leaflet.css'},
				{ style: 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css' },
				{ style: 'https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css'}
			]
        }    
    }
	res.renderVue(template, data, req.vueOptions);
}


var sync = async function(uuid) {
	
	var profile = await uber.user.getProfileAsync();
	var trips = await uber.user.getHistoryAsync(0, 50);
	await client.hsetAsync(uuid, 'profile', JSON.stringify(profile));
	await client.hsetAsync(uuid, 'trips', JSON.stringify(trips));
	return;
}

var resync = async function(req, res, next) {
	client.del(req.cookies.user);
	res.redirect('/');
}

var auth = async function (req, res, next) {
	if (req.query.error) {
		res.send(req.query).end()
	}
	uber.authorizationAsync({ authorization_code: req.query.code })
		.then(function () {
			sync(req.cookies.user)
			.then(function() {
				res.redirect('/');
			});
		})
		.error(function (err) {
			console.error(err);
		});
}


module.exports = { auth, view, resync }