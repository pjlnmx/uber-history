var express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'),
	cors = require('cors'),
	expressVue = require("express-vue"),
	uber = require('./uber'),
	cookieParser = require('cookie-parser');

const port = process.env.PORT || 3000;
const vueOptions = {
    rootPath: path.join(__dirname, './')
}
const expressVueMiddleware = expressVue.init(vueOptions);
const app = express();
app.use(expressVueMiddleware);
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cookieParser())
app.use(cors());
app.get('/auth', uber.auth);
app.get('/resync', uber.resync);
app.get('/', uber.view);
app.listen(port, () => {
	console.log('Listening on port ' + port);
});
